#!/bin/bash

# Makes sure directories can be exported, then 
#  runs 'git daemon' with absolute paths

set -e

[ "$#" -gt 0 ] || { echo >&2 "Usage: $0 <repository> ..."; exit 5; }

BASE="`pwd`"

dirs=()

for d in "$@"; do
  cd "$BASE"
  cd "$d"

  gitdir="`git rev-parse --git-dir`"
  cd "$gitdir"

  abs="`pwd`"

  GDEO="$abs/git-daemon-export-ok"


  [ -f "$GDEO" ] || { echo "Missing: $GDEO"; exit 10; }

  dirs=("${dirs[@]}" "$abs")
done

for d in "${dirs[@]}"; do
  echo 'git://'`hostname`"$d" | sed 's,/\.git$,,'
done

cd /
git daemon --listen=0.0.0.0 --reuseaddr "${dirs[@]}"
